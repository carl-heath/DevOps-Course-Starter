from flask import Flask, render_template, request, redirect

from todo_app.flask_config import Config
from todo_app.data.session_items import get_items, add_item
import todo_app.data.session_items as session_items

app = Flask(__name__)
app.config.from_object(Config())


@app.route('/')
def index():
    items = session_items.get_items()
    return render_template("index.html", items_list = items)

@app.route('/addtodo', methods=['POST'])
def add_todo_item():
    add_item (request.form.get('todo_itemname','todo_priority'))
    return redirect('/')